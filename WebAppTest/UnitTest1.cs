using System;
using WebApp;
using Xunit;

namespace WebAppTest
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            Assert.Equal(1, 1);
        }

        [Fact]
        public void Test2()
        {
            FakeClass fc = new FakeClass();
            Assert.NotNull(fc);
        }
    }
}
